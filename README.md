# EposPod

[![CI Status](https://img.shields.io/travis/scnigami/EposPod.svg?style=flat)](https://travis-ci.org/scnigami/EposPod)
[![Version](https://img.shields.io/cocoapods/v/EposPod.svg?style=flat)](https://cocoapods.org/pods/EposPod)
[![License](https://img.shields.io/cocoapods/l/EposPod.svg?style=flat)](https://cocoapods.org/pods/EposPod)
[![Platform](https://img.shields.io/cocoapods/p/EposPod.svg?style=flat)](https://cocoapods.org/pods/EposPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

EposPod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'EposPod'
```

## Author

scnigami, scnigami@gmail.com

## License

EposPod is available under the MIT license. See the LICENSE file for more info.
