//
//  EposViewController.m
//  EposPod
//
//  Created by scnigami on 05/15/2019.
//  Copyright (c) 2019 scnigami. All rights reserved.
//

#import "EposViewController.h"
#import <ePOS2.h>

@interface EposViewController ()<Epos2DiscoveryDelegate>

@end

@implementation EposViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // start discovery
    int result = EPOS2_SUCCESS;
    Epos2FilterOption *filterOption = [[Epos2FilterOption alloc] init];
    [filterOption setPortType:EPOS2_PORTTYPE_ALL];
    [filterOption setDeviceModel:EPOS2_MODEL_ALL];
    [filterOption setDeviceType:EPOS2_TYPE_ALL];
    
    result = [Epos2Discovery start:filterOption delegate:self];
    if (result != EPOS2_SUCCESS) {
        return;
    }
}

- (void) onDiscovery:(Epos2DeviceInfo *)deviceInfo
{
    NSString *deviceName = deviceInfo.deviceName;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
