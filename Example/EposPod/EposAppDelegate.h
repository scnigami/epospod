//
//  EposAppDelegate.h
//  EposPod
//
//  Created by scnigami on 05/15/2019.
//  Copyright (c) 2019 scnigami. All rights reserved.
//

@import UIKit;

@interface EposAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
