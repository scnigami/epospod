#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "ePOS2.h"
#import "ePOSBluetoothConnection.h"
#import "ePOSEasySelect.h"
#import "ePOSPrint.h"

FOUNDATION_EXPORT double EposPodVersionNumber;
FOUNDATION_EXPORT const unsigned char EposPodVersionString[];

